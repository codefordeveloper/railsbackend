# Note model has been created to talk with database,
# store and vlidate data, perform the business logic
# and otherwise do the heavy lifting.
class Note < ApplicationRecord
  self.inheritance_column = :type
  belongs_to :topic, polymorphic: true, optional: true
end
