# Task model has been created
class Task < ApplicationRecord
  enum status: [:active, :completed]
  belongs_to :offer
  belongs_to :project
end
