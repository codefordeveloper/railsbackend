# Now we don't have to inherit manually from ActiveRecord::Base.
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
