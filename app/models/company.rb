# Company model has been created.
class Company < ApplicationRecord
  validates :name, presence: true
end
