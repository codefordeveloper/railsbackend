# Active model serializer is used to generate JSON API in rails.
# We want to provide a JSON API to go alongside the HTML view so that
# if we append a .json to the URL we'll get the article's data.
class OfferSerializer < ActiveModel::Serializer
  attributes :id, :name, :price, :status, :valid_until
  has_many :tasks
  has_many :contacts
  has_one :project
end
