# Active model serializer is used to generate JSON API in rails.
# We want to provide a JSON API to go alongside the HTML view so that
# if we append a .json to the URL we'll get the article's data.
class CompanySerializer < ActiveModel::Serializer
  attributes :id, :name, :phone, :email, :website,
  :address, :customer_id, :additional_info
end
