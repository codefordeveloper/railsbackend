# Active model serializer is used to generate JSON API in rails.
# We want to provide a JSON API to go alongside the HTML view so that
# if we append a .json to the URL we'll get the article's data.
class TaskSerializer < ActiveModel::Serializer
  attributes :id, :description, :status, :due_at
  has_one :project
  has_one :offer
end
