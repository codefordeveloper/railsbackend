# Active model serializer is used to generate JSON API in rails.
# We want to provide a JSON API to go alongside the HTML view so that
# if we append a .json to the URL we'll get the article's data.
class NoteSerializer < ActiveModel::Serializer
  attributes :id, :type, :title, :description
  has_many :topic
end
