# Active model serializer is used to generate JSON API in rails.
# We want to provide a JSON API to go alongside the HTML view so that
# if we append a .json to the URL we'll get the article's data.
# /contacts/1.json
class ContactSerializer < ActiveModel::Serializer
  attributes :id, :family_name, :given_names, :title, :phone,
  :website, :address, :customer_id, :additional_info

  has_one :company
  has_many :projects
  has_many :notes
end
