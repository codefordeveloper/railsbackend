# Contact controller has been created for relatioship between
# companies and projects.
class ContactsController < ApplicationController
  # GET /contacts (GET request means retrive information of all contacts)
  def index
    @contacts = Contact.all
    render json: @contacts
  end

  # GET /contacts/1 (Retrive a contact with an ID of 1)
  def show
    render json: @contact
  end

  # POST /contacts (create new contact)
  def create
    @contact = Contact.new(contact_params)
    @contact.company = relationship_params[:company]
    @contact.offers = relationship_params[:offers] || []
    @contact.projects = relationship_params[:projects] || []

    if @contact.save
      render json: @contact, status: :created, location: @contact
    else
      render json: @contact.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /contacts/1
  def update
      @contact.company = relationship_params[:company] if relationship_params.has_key? :company
      @contact.offers = relationship_params[:offers] if relationship_params[:offers]
      @contact.projects = relationship_params[:projects] if relationship_params[:projects]
      
    if @contact.update(contact_params)  
      render json: @contact
    else
      render json: @contact.errors, status: :unprocessable_entity
    end
  end

  # DELETE /contact/1
  def destroy
    @contact.destroy
  end

  private

  # Use callback to share common setup or constraints between actions.
  def set_contact
    @contact = Contact.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def contact_params
    params.require(:data).require(:attribute).permit(
      :family_name, :given_name, :company_id, :title, :phone, :email,
      :website, :address, :customer_id, :addtional_info
    )
  end
end
