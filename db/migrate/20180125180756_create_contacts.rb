class CreateContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :contacts do |t|
      t.string :family_name
      t.string :given_name
      t.references :company, index: true, foreign_key: true
      t.string :title
      t.string :phone
      t.string :email
      t.text :website
      t.text :address
      t.string :customer_id
      t.text :addtional_info
      t.timestamps
    end
  end
end
