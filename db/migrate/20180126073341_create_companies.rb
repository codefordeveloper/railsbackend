# migration file for companies has been created.
class CreateCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.text :website
      t.text :address
      t.string :customer_id
      t.text :addtional_info
      t.timestamps
    end
  end
end
